//
//  ItemsSceneUITests.swift
//  CleanArchitectureExampleUITests
//
//  Created by Xavier Mellado Esteban on 3/6/22.
//

import XCTest

class ItemsSceneUITests: XCTestCase {

    var app: XCUIApplication!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure
        // it happens for each test method.
        app = XCUIApplication()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.

        app.launch()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_items_areShown() throws {
        // Given
        let firstCell = app.tables.cells.element(boundBy: 0)
        let image = firstCell.images[AccessibilityIdentifiers.ItemTableViewCell.image]
        let title = firstCell.staticTexts[AccessibilityIdentifiers.ItemTableViewCell.title]

        // Then
        // With hittable, it is checked that the element is on screen and shown to the user.
        // See: https://stackoverflow.com/a/33248181/5189200
        // To extract the actual text from a static label, set the 'Accessibility Identifier' to
        // retrieve it, but never the 'Accessibility Label', which will contain the actual displayed
        // text. Do not use 'xxx.value as? String', since it will always return an empty string.
        // See: https://stackoverflow.com/a/44475117/5189200
        XCTAssertTrue(image.isHittable,
                      "First item image is not shown")
        XCTAssertTrue(title.isHittable,
                      "First item title is not shown")
        XCTAssertTrue(!title.label.isEmpty,
                      "First item title was not set")
    }

    func test_navigationTo_itemDetailIsWorking() {
        // Given
        let firstCell = app.tables.cells.element(boundBy: 0)

        // When
        firstCell.tap()

        // Then
        // Use waitForExistence for dealing with animations. If the element exists, then the method
        // returns immediately and the test passes, but if it doesn't then the method will wait for
        // up to one second.
        // See: https://www.hackingwithswift.com/articles/148/xcode-ui-testing-cheat-sheet
        XCTAssertTrue(app.images[AccessibilityIdentifiers.ItemDetail.image].waitForExistence(timeout: 1),
                      "First item detail is not shown")
    }
}
