//
//  ItemDetailSceneUITests.swift
//  CleanArchitectureExampleUITests
//
//  Created by Xavier Mellado Esteban on 9/6/22.
//

import XCTest

class ItemDetailSceneUITests: XCTestCase {

    var app: XCUIApplication!

    override func setUpWithError() throws {
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()
    }

    override func tearDownWithError() throws {
    }

    func test_itemDetail_isShown() {
        // Given
        let firstCell = app.tables.cells.element(boundBy: 0)

        // When
        firstCell.tap()

        // Then
        let title = app.navigationBars.firstMatch.staticTexts.firstMatch // Gets navigation item label
        let image = app.images[AccessibilityIdentifiers.ItemDetail.image]

        XCTAssertTrue(title.isHittable,
                      "Item detail name is not shown")
        XCTAssertTrue(!title.label.isEmpty,
                      "Item detail name was not set")
        XCTAssertTrue(image.isHittable,
                      "Item detail image is not shown")
    }
}
