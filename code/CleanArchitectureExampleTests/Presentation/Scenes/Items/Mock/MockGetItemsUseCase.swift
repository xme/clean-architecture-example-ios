//
//  MockGetItemsUseCase.swift
//  CleanArchitectureExampleTests
//
//  Created by Xavier Mellado Esteban on 9/6/22.
//

@testable import CleanArchitectureExample
import Combine

class MockGetItemsUseCase: GetItemsUseCaseProtocol {

    var executeCounter = 0
    var items = [Item]()
    var error: Error?

    func execute(with params: Void) -> AnyPublisher<[Item], Error> {
        executeCounter += 1

        if let outputError = error {
            return Fail(error: outputError).eraseToAnyPublisher()
        }

        return Just(items).setFailureType(to: Error.self).eraseToAnyPublisher()
    }
}
