//
//  ItemsSceneTests.swift
//  CleanArchitectureExampleTests
//
//  Created by Xavier Mellado Esteban on 9/6/22.
//

@testable import CleanArchitectureExample
import XCTest
import Combine

class ScenesItemsViewModelTests: XCTestCase {

    private var cancellables: Set<AnyCancellable>!

    override func setUp() {
        super.setUp()

        cancellables = []
    }

    override func tearDown() {
        super.tearDown()
        
        cancellables.forEach { $0.cancel() }
        cancellables.removeAll()
    }

    func test_getItems_withSuccess_returnsSuccessStateWithItems() throws {
        // Given
        var voidCounter = 0
        var loadingCounter = 0
        var successCounter = 0
        var failureCounter = 0
        let expectedItems = TestItemFactory.makeItemList(count: 5)
        let mockGetItemsUseCase = TestItemsSceneFactory.makeMockGetItemsUseCase()
        mockGetItemsUseCase.items = expectedItems
        let itemsViewModel = TestItemsSceneFactory.makeItemsViewModel(getItemsUseCase: mockGetItemsUseCase)
        var actualItems: [Item]?

        // See: https://www.swiftbysundell.com/articles/unit-testing-combine-based-swift-code/
        let expectation = self.expectation(description: "Get items operation")
        itemsViewModel.itemsDriver
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure:
                    XCTFail("Failure was received")
                case .finished:
                    XCTFail("Completion was received")
                    break
                }
            }, receiveValue: { itemsState in
                switch itemsState {
                case .void:
                    voidCounter += 1
                case .loading:
                    loadingCounter += 1
                case .success(let items):
                    successCounter += 1
                    actualItems = items
                    expectation.fulfill()
                case .failure:
                    failureCounter += 1
                    expectation.fulfill()
                }
            })
            .store(in: &cancellables)

        // When
        itemsViewModel.getItems()

        // Then
        wait(for: [expectation], timeout: 0.1)

        XCTAssertEqual(1, voidCounter,
                       "Items view model does not publish void state exactly one time")
        XCTAssertEqual(1, loadingCounter,
                       "Items view model does not publish loading state exactly one time")
        XCTAssertEqual(1, successCounter,
                       "Items view model does not publish success state exactly one time")
        XCTAssertEqual(0, failureCounter,
                       "Items view model published failure state instead of success state")
        XCTAssertEqual(expectedItems, actualItems,
                       "Expected and actual items are not equal")
    }

    func test_getItems_withError_returnsFailureStateWithError() throws {
        // Given
        var voidCounter = 0
        var loadingCounter = 0
        var successCounter = 0
        var failureCounter = 0
        let mockGetItemsUseCase = TestItemsSceneFactory.makeMockGetItemsUseCase()
        let expectedError = TestError.dummyError
        mockGetItemsUseCase.error = expectedError
        let itemsViewModel = TestItemsSceneFactory.makeItemsViewModel(getItemsUseCase: mockGetItemsUseCase)
        var actualError: Error?

        // See: https://medium.com/vincit/unit-testing-rxswift-application-f0c6ea460429
        let expectation = self.expectation(description: "Get items operation")
        itemsViewModel.itemsDriver
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure:
                    XCTFail("Failure was received")
                case .finished:
                    XCTFail("Completion was received")
                    break
                }
            }, receiveValue: { itemsState in
                switch itemsState {
                case .void:
                    voidCounter += 1
                case .loading:
                    loadingCounter += 1
                case .success:
                    successCounter += 1
                    expectation.fulfill()
                case .failure(let error):
                    failureCounter += 1
                    actualError = error
                    expectation.fulfill()
                }
            })
            .store(in: &cancellables)

        // When
        itemsViewModel.getItems()

        // Then
        wait(for: [expectation], timeout: 0.1)

        XCTAssertEqual(1, voidCounter,
                       "Items view model does not publish void state exactly one time")
        XCTAssertEqual(1, loadingCounter,
                       "Items view model does not publish loading state exactly one time")
        XCTAssertEqual(0, successCounter,
                       "Items view model published success state instead of failure state")
        XCTAssertEqual(1, failureCounter,
                       "Signin view model does not publish failure state exactly one time")
        XCTAssertTrue(actualError is TestError,
                      "Actual and expected error types are different")
        XCTAssertEqual(expectedError, actualError as? TestError,
                       "Expected and actual errors are not equal")
    }
}
