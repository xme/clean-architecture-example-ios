//
//  TestItemsSceneFactory.swift
//  CleanArchitectureExampleTests
//
//  Created by Xavier Mellado Esteban on 9/6/22.
//

@testable import CleanArchitectureExample

struct TestItemsSceneFactory {

    static func makeMockGetItemsUseCase() -> MockGetItemsUseCase {
        return MockGetItemsUseCase()
    }

    static func makeItemsViewModel(getItemsUseCase: GetItemsUseCaseProtocol) -> ItemsViewModel {
        return ItemsViewModel(getItemsUseCase: getItemsUseCase)
    }
}
