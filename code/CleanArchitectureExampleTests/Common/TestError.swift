//
//  TestError.swift
//  CleanArchitectureExampleTests
//
//  Created by Xavier Mellado Esteban on 9/6/22.
//

enum TestError: Error {
    case dummyError
}
