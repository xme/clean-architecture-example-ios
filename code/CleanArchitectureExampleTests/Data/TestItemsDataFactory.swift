//
//  TestItemsDataFactory.swift
//  CleanArchitectureExampleTests
//
//  Created by Xavier Mellado Esteban on 8/6/22.
//

@testable import CleanArchitectureExample

struct TestItemsDataFactory {

    static func makeMockRemoteItemsDataSource() -> MockRemoteItemsDataSource {
        return MockRemoteItemsDataSource()
    }

    static func makeMockLocalItemsDataSource() -> MockLocalItemsDataSource {
        return MockLocalItemsDataSource()
    }

    static func makeItemsDataSourceFactory(remoteItemsDataSource: RemoteItemsDataSourceProtocol,
                                           localItemsDataSource: LocalItemsDataSourceProtocol) -> ItemsDataSourceFactory {
        return ItemsDataSourceFactory(remoteItemsDataSource: remoteItemsDataSource,
                                      localItemsDataSource: localItemsDataSource)
    }

    static func makeItemsRepository(remoteItemsDataSource: RemoteItemsDataSourceProtocol,
                                    localItemsDataSource: LocalItemsDataSourceProtocol) -> ItemsRepository {
        let itemsDataSourceFactory = makeItemsDataSourceFactory(remoteItemsDataSource: remoteItemsDataSource,
                                                                localItemsDataSource: localItemsDataSource)
        return ItemsRepository(itemsDataSourceFactory: itemsDataSourceFactory)
    }
}
