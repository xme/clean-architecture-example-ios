//
//  MockLocalItemsDataSource.swift
//  CleanArchitectureExampleTests
//
//  Created by Xavier Mellado Esteban on 8/6/22.
//

@testable import CleanArchitectureExample
import Combine

class MockLocalItemsDataSource: LocalItemsDataSourceProtocol {

    var getItemsCounter = 0
    var saveItemsCounter = 0
    var isValidCacheCounter = 0
    var clearCounter = 0
    var validCache = false
    var items = [Item]()

    func getItems() -> AnyPublisher<[Item], Error> {
        getItemsCounter += 1

        return Just(items).setFailureType(to: Error.self).eraseToAnyPublisher()
    }

    func isValidCache() -> AnyPublisher<Bool, Error> {
        isValidCacheCounter += 1

        return Just(validCache).setFailureType(to: Error.self).eraseToAnyPublisher()
    }

    func saveItems(_ items: [Item]) -> AnyPublisher<Void, Error> {
        saveItemsCounter += 1

        return Just(()).setFailureType(to: Error.self).eraseToAnyPublisher()
    }

    func clear() -> AnyPublisher<Void, Error> {
        clearCounter += 1

        return Just(()).setFailureType(to: Error.self).eraseToAnyPublisher()
    }
}
