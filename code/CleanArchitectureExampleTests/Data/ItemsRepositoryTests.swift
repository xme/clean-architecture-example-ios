//
//  ItemsRepositoryTests.swift
//  CleanArchitectureExampleTests
//
//  Created by Xavier Mellado Esteban on 8/6/22.
//

@testable import CleanArchitectureExample
import XCTest

class ItemsRepositoryTests: XCTestCase {

    func test_getItems_withNoValidCache_returnsRemoteItems() throws {
        // Given
        let expectedItems = TestItemFactory.makeItemList(count: 5)
        let mockRemoteItemsDataSource = TestItemsDataFactory.makeMockRemoteItemsDataSource()
        mockRemoteItemsDataSource.items = expectedItems
        let mockLocalItemsDataSource = TestItemsDataFactory.makeMockLocalItemsDataSource()
        mockLocalItemsDataSource.validCache = false
        let itemsRepository = TestItemsDataFactory
            .makeItemsRepository(remoteItemsDataSource: mockRemoteItemsDataSource,
                            localItemsDataSource: mockLocalItemsDataSource)

        // When
        let actualItems = try awaitPublisher(itemsRepository.getItems())

        // Then
        XCTAssertEqual(1, mockLocalItemsDataSource.isValidCacheCounter,
                      "Is valid cache is not called once")
        XCTAssertEqual(1, mockRemoteItemsDataSource.getItemsCounter,
                      "Remote items is not called once")
        XCTAssertEqual(1, mockLocalItemsDataSource.saveItemsCounter,
                      "Items are not saved to cache once")
        XCTAssertEqual(0, mockLocalItemsDataSource.getItemsCounter,
                      "Get local items is called")
        XCTAssertEqual(expectedItems, actualItems,
                      "Expected and actual items are not equal")
    }

    func test_getItems_withValidCache_returnsLocalItems() throws {
        // Given
        let expectedItems = TestItemFactory.makeItemList(count: 5)
        let mockRemoteItemsDataSource = TestItemsDataFactory.makeMockRemoteItemsDataSource()
        let mockLocalItemsDataSource = TestItemsDataFactory.makeMockLocalItemsDataSource()
        mockLocalItemsDataSource.items = expectedItems
        mockLocalItemsDataSource.validCache = true
        let itemsRepository = TestItemsDataFactory
            .makeItemsRepository(remoteItemsDataSource: mockRemoteItemsDataSource,
                            localItemsDataSource: mockLocalItemsDataSource)

        // When
        let actualItems = try awaitPublisher(itemsRepository.getItems())

        // Then
        XCTAssertEqual(1, mockLocalItemsDataSource.isValidCacheCounter,
                      "Is valid cache is not called once")
        XCTAssertEqual(0, mockRemoteItemsDataSource.getItemsCounter,
                      "Get remote items is called")
        XCTAssertEqual(0, mockLocalItemsDataSource.saveItemsCounter,
                      "Items are saved to cache")
        XCTAssertEqual(1, mockLocalItemsDataSource.getItemsCounter,
                      "Get local items is not called once")
        XCTAssertEqual(expectedItems, actualItems,
                      "Expected and actual items are not equal")
    }
}
