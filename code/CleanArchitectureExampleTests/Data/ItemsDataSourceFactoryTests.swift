//
//  ItemsDataSourceFactoryTests.swift
//  CleanArchitectureExampleTests
//
//  Created by Xavier Mellado Esteban on 8/6/22.
//

@testable import CleanArchitectureExample
import XCTest

class ItemsDataSourceFactoryTests: XCTestCase {

    func test_locateDataSource_withNoValidCache_returnsRemoteService() {
        // Given
        let mockRemoteItemsDataSource = TestItemsDataFactory.makeMockRemoteItemsDataSource()
        let mockLocalItemsDataSource = TestItemsDataFactory.makeMockLocalItemsDataSource()
        let itemsDataSourceFactory = TestItemsDataFactory
            .makeItemsDataSourceFactory(remoteItemsDataSource: mockRemoteItemsDataSource,
                                        localItemsDataSource: mockLocalItemsDataSource)

        // When
        let dataSource = itemsDataSourceFactory.locateDataSource(when: false)

        // Then
        XCTAssertTrue(dataSource is MockRemoteItemsDataSource,
                      "Factory does not return remote data source when cache is not valid")
    }

    func test_locateDataSource_withValidCache_returnsCacheService() {
        // Given
        let mockRemoteItemsDataSource = TestItemsDataFactory.makeMockRemoteItemsDataSource()
        let mockLocalItemsDataSource = TestItemsDataFactory.makeMockLocalItemsDataSource()
        let itemsDataSourceFactory = TestItemsDataFactory
            .makeItemsDataSourceFactory(remoteItemsDataSource: mockRemoteItemsDataSource,
                                        localItemsDataSource: mockLocalItemsDataSource)

        // When
        let dataSource = itemsDataSourceFactory.locateDataSource(when: true)

        // Then
        XCTAssertTrue(dataSource is MockLocalItemsDataSource,
                      "Factory does not return local data source when cache is valid")
    }
}
