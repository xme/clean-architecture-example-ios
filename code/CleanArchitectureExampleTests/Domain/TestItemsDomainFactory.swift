//
//  TestItemsDomainFactory.swift
//  CleanArchitectureExampleTests
//
//  Created by Xavier Mellado Esteban on 9/6/22.
//

@testable import CleanArchitectureExample
import Combine

struct TestItemsDomainFactory {

    static func makeMockItemsRepository() -> MockItemsRepository {
        return MockItemsRepository()
    }

    static func makeGetItemsUseCase(itemsRepository: ItemsRepositoryProtocol) -> GetItemsUseCase {
        return GetItemsUseCase(itemsRepository: itemsRepository)
    }
}
