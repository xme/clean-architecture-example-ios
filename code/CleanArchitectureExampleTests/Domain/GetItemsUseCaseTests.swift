//
//  GetItemsUseCaseTests.swift
//  CleanArchitectureExampleTests
//
//  Created by Xavier Mellado Esteban on 9/6/22.
//

@testable import CleanArchitectureExample
import XCTest

class DomainGetItemsUseCaseTests: XCTestCase {

    func test_getItems_returnsItems() throws {
        // Given
        let expectedItems = TestItemFactory.makeItemList(count: 5)
        let mockItemsRepository = TestItemsDomainFactory.makeMockItemsRepository()
        mockItemsRepository.items = expectedItems
        let getItemsUseCase = TestItemsDomainFactory.makeGetItemsUseCase(itemsRepository: mockItemsRepository)

        // When
        let actualItems = try awaitPublisher(getItemsUseCase.execute(with: ()))

        // Then
        XCTAssertEqual(1, mockItemsRepository.getItemsCounter,
                      "Get items in repository is not called once")
        XCTAssertEqual(expectedItems, actualItems,
                      "Expected and actual items are not equal")
    }
}
