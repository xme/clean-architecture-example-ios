//
//  MockItemsRepository.swift
//  CleanArchitectureExampleTests
//
//  Created by Xavier Mellado Esteban on 9/6/22.
//

@testable import CleanArchitectureExample
import Combine

class MockItemsRepository: ItemsRepositoryProtocol {

    var getItemsCounter = 0
    var items = [Item]()

    func getItems() -> AnyPublisher<[Item], Error> {
        getItemsCounter += 1

        return Just(items).setFailureType(to: Error.self).eraseToAnyPublisher()
    }
}
