//
//  LocalItemsDataSourceTests.swift
//  CleanArchitectureExampleTests
//
//  Created by Xavier Mellado Esteban on 8/6/22.
//

@testable import CleanArchitectureExample
import Combine
import XCTest

class LocalItemsDataSourceTests: XCTestCase {

    private var localItemsDataSource: LocalItemsDataSource!

    override func setUpWithError() throws {
        try super.setUpWithError()

        localItemsDataSource = TestItemsDataSourceFactory.makeLocalItemsDataSource()
        // Cache should be deleted before running first test to avoid any dependency with previous tests
        try awaitPublisher(localItemsDataSource.clear())
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()

        // Cache file should be deleted after each test to make them independent
        try awaitPublisher(localItemsDataSource.clear())
    }

    func test_isValidCache_returnsFalseAfterInitialization() throws {
        // Given

        // When
        let validCache = try awaitPublisher(localItemsDataSource.isValidCache())

        // Then
        XCTAssertFalse(validCache,
                       "Cache is valid after initializing and having not saved items")
    }

    func test_isValidCache_returnsTrueAfterSaving() throws {
        // Given
        let expectedItems = TestItemFactory.makeItemList(count: 1)
        try! awaitPublisher(localItemsDataSource.saveItems(expectedItems))

        // When
        let validCache = try awaitPublisher(localItemsDataSource.isValidCache())

        // Then
        XCTAssertTrue(validCache,
                      "Is valid cache does not return true after saving an item")
    }

    func test_isValidCache_returnsFalseAfterClearing() throws {
        // Given
        let expectedItems = TestItemFactory.makeItemList(count: 1)
        try awaitPublisher(localItemsDataSource.saveItems(expectedItems))
        try awaitPublisher(localItemsDataSource.clear())

        // When
        let validCache = try awaitPublisher(localItemsDataSource.isValidCache())

        // Then
        XCTAssertFalse(validCache,
                       "After saving items and then clear them, cache is still valid")
    }

    func test_saveItems_storesData() throws {
        // Given
        let expectedItems = TestItemFactory.makeItemList(count: 1)

        // When
        try awaitPublisher(localItemsDataSource.saveItems(expectedItems))

        // Then
        let actualItems = try awaitPublisher(localItemsDataSource.getItems())
        XCTAssertTrue(0 < actualItems.count,
                      "Expected items are not stored")
    }

    func test_getItems_returnsSavedItems() throws {
        // Given
        let expectedItems = TestItemFactory.makeItemList(count: 5)
        try awaitPublisher(localItemsDataSource.saveItems(expectedItems))

        // When
        let actualItems = try awaitPublisher(localItemsDataSource.getItems())

        // Then
        // Beware of array comparison: https://stackoverflow.com/a/27580740/5189200
        XCTAssertEqual(expectedItems, actualItems,
                      "Expected and actual items are not equal")
    }

    func test_clear_deletesData() throws {
        // Given
        let expectedItems = TestItemFactory.makeItemList(count: 1)
        var thrownError: Error?
        try awaitPublisher(localItemsDataSource.saveItems(expectedItems))

        // When
        try awaitPublisher(localItemsDataSource.clear())

        // Then
        // See: https://www.swiftbysundell.com/articles/testing-error-code-paths-in-swift/
        // Capture the thrown error using a closure
        XCTAssertThrowsError(try awaitPublisher(localItemsDataSource.getItems())) {
            thrownError = $0
        }

        // First we’ll verify that the error is of the right type, to make debugging easier in case of failures.
        XCTAssertTrue(thrownError is LocalItemsDataSource.CacheError,
                      "Unexpected error type: \(type(of: thrownError))")

        // Verify that our error is equal to what we expect
        XCTAssertEqual(thrownError as? LocalItemsDataSource.CacheError, .cacheNotAvailableError,
                       "After saving items and then clear them, cache is not returning a cache not available error when requesting items again")
    }
}
