//
//  RemoteItemMapperTests.swift
//  CleanArchitectureExampleTests
//
//  Created by Xavier Mellado Esteban on 12/6/22.
//

@testable import CleanArchitectureExample
import XCTest

class RemoteItemMapperTests: XCTestCase {

    func test_mapFromRemote_toDomainIsWorking() {
        // Given
        let networkItem = TestItemFactory.makeRemoteItem()

        // When
        let item = TestItemsDataSourceFactory.makeRemoteItemMapper().map(networkItem)

        // Then
        assertItemDataEquality(expectedItem: networkItem, actualItem: item)
    }

    private func assertItemDataEquality(expectedItem: RemoteItem, actualItem: Item) {
        XCTAssertEqual(expectedItem.name, actualItem.name,
                      "Item name is not properly mapped")
        XCTAssertEqual(expectedItem.url, actualItem.url,
                      "Item URL is not properly mapped")
    }
}
