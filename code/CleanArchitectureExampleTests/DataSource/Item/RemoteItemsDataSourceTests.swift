//
//  RemoteItemsDataSourceTests.swift
//  CleanArchitectureExampleTests
//
//  Created by Xavier Mellado Esteban on 8/6/22.
//

@testable import CleanArchitectureExample
import Combine
import XCTest

class RemoteItemsDataSourceTests: XCTestCase {

    private var remoteItemsDataSource: RemoteItemsDataSource!

    override func setUpWithError() throws {
        try super.setUpWithError()

        let remoteItemMapper = TestItemsDataSourceFactory.makeRemoteItemMapper()
        remoteItemsDataSource = TestItemsDataSourceFactory.makeRemoteItemsDataSource(remoteItemMapper: remoteItemMapper)
    }

    // This test is silly, because there is no network layer. It was added just for the sake of completion.
    func test_getItems_returnsData() throws {
        // Given

        // When
        let actualItems = try awaitPublisher(remoteItemsDataSource.getItems())

        // Then
        XCTAssertTrue(0 < actualItems.count,
                      "No items are returned")
    }
}
