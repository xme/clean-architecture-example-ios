//
//  TestItemsDataSourceFactory.swift
//  CleanArchitectureExampleTests
//
//  Created by Xavier Mellado Esteban on 8/6/22.
//

@testable import CleanArchitectureExample
import Foundation

struct TestItemsDataSourceFactory {

    static func makeRemoteItemMapper() -> RemoteItemMapper {
        return RemoteItemMapper()
    }

    static func makeRemoteItemsDataSource(remoteItemMapper: RemoteItemMapper) -> RemoteItemsDataSource {
        return RemoteItemsDataSource(remoteItemMapper: remoteItemMapper)
    }

    static func makeLocalItemsDataSource() -> LocalItemsDataSource {
        return LocalItemsDataSource()
    }
}
