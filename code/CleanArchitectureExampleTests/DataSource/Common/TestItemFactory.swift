//
//  TestItemFactory.swift
//  CleanArchitectureExampleTests
//
//  Created by Xavier Mellado Esteban on 8/6/22.
//

@testable import CleanArchitectureExample

struct TestItemFactory {

    static func makeItem() -> Item {
        return Item(
            name: TestValueFactory.makeRandomUUID(),
            url: TestValueFactory.makeURL()
        )
    }

    static func makeRemoteItem() -> RemoteItem {
        return RemoteItem(
            name: TestValueFactory.makeRandomUUID(),
            url: TestValueFactory.makeURL()
        )
    }

    static func makeItemList(count: UInt) -> [Item] {
        var items = [Item]()

        for _ in 0..<count {
            items.append(makeItem())
        }

        return items
    }
}
