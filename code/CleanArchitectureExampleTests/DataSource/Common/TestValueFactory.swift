//
//  TestValueFactory.swift
//  CleanArchitectureExampleTests
//
//  Created by Xavier Mellado Esteban on 8/6/22.
//

import Foundation

struct TestValueFactory {

    static func makeRandomUUID() -> String {
        return UUID().uuidString
    }

    static func makeURL() -> String {
        return "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/The-Hobbit-Over-Hill-and-Under-Hill/Gollum.jpg"
    }
}
