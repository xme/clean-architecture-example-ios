//
//  UseCase.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 6/6/22.
//

import Combine
import Dispatch

/// The generic abstract class for modeling a use case with the [template pattern](https://en.wikipedia.org/wiki/Template_method_pattern)
/// for implementing thread control, leaving to concrete classes the implementation of
/// buildUseCasePublisher method.
///
/// - Parameters:
///     - P: The *parameter* class containing all inputs needed.
///     - R: The *result* class.
///     - E: The *error* class.
class UseCase<P, R, E> where E: Error {

    private let executionScheduler: DispatchQueue
    private let publicationScheduler: DispatchQueue

    init(executionScheduler: DispatchQueue = Scheduler.backgroundScheduler,
         publicationScheduler: DispatchQueue = Scheduler.mainScheduler) {
        self.executionScheduler = executionScheduler
        self.publicationScheduler = publicationScheduler
    }

    // MARK: Execution

    // Execute function cannot be overridden, since it is the template method. Subclasses will define what will be
    // published implementing buildUseCasePublisher.
    // See: https://en.wikipedia.org/wiki/Template_method_pattern
    final func execute(with params: P) -> AnyPublisher<R, E> {
        return buildUseCasePublisher(with: params)
            // Execute requests in background thread
            .subscribe(on: executionScheduler)
            // Publish results in main thread
            .receive(on: publicationScheduler)
            // Apply type erasure on the chain of operators returning a AnyPublisher
            .eraseToAnyPublisher()
    }

    // Helper method to be overridden by subclasses
    func buildUseCasePublisher(with params: P) -> AnyPublisher<R, E> {
        fatalError("buildUseCasePublisher() must be overridden in child classes. This is an abstract method.")
    }
}
