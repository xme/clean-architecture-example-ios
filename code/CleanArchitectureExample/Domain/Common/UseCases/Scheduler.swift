//
//  Scheduler.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 6/6/22.
//

import Dispatch

struct Scheduler {

    static let backgroundScheduler = DispatchQueue.global(qos: .userInitiated)
    static let mainScheduler = DispatchQueue.main
}
