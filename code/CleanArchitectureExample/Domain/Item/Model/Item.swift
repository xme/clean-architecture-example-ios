//
//  File.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 3/6/22.
//

struct Item: Equatable {

    let name: String
    let url: String
}
