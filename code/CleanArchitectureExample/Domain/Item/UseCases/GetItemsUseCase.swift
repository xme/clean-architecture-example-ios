//
//  GetItems.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 6/6/22.
//

import Combine
import Dispatch

protocol GetItemsUseCaseProtocol {

    // This execute() function is needed in the protocol and must match the ABCDUseCase<P, R> definition below, in order
    // to allow testing, since mock use cases will be needed. This protocol cannot use generics since a protocol with
    // generics is not a type, and the view model will not be allowed to have a property of type ABCDUseCaseProtocol<P, R>.
    func execute(with params: Void) -> AnyPublisher<[Item], Error>
}

final class GetItemsUseCase: UseCase<Void, [Item], Error>,
                             GetItemsUseCaseProtocol {

    private let itemsRepository: ItemsRepositoryProtocol

    init(itemsRepository: ItemsRepositoryProtocol,
         executionScheduler: DispatchQueue = Scheduler.backgroundScheduler,
         publicationScheduler: DispatchQueue = Scheduler.mainScheduler) {
        self.itemsRepository = itemsRepository

        super.init(executionScheduler: executionScheduler, publicationScheduler: publicationScheduler)
    }

    override func buildUseCasePublisher(with params: Void) -> AnyPublisher<[Item], Error> {
        return itemsRepository.getItems()
    }
}
