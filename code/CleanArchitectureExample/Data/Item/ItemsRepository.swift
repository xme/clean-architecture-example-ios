//
//  ItemsRepository.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 7/6/22.
//

import Combine

protocol ItemsRepositoryProtocol {

    func getItems() -> AnyPublisher<[Item], Error>
}

struct ItemsRepository: ItemsRepositoryProtocol {

    private let itemsDataSourceFactory: ItemsDataSourceFactoryProtocol

    init(itemsDataSourceFactory: ItemsDataSourceFactoryProtocol) {
        self.itemsDataSourceFactory = itemsDataSourceFactory
    }
    
    func getItems() -> AnyPublisher<[Item], Error> {
        return itemsDataSourceFactory.localItemsDataSource.isValidCache()
            .flatMap { (validCache: Bool) -> AnyPublisher<[Item], Error> in
                // Get datasource based on whether local data is valid
                let itemsDataSource = self.itemsDataSourceFactory.locateDataSource(when: validCache)

                // Get data
                var itemsPublisher = itemsDataSource.getItems()
                if !validCache {
                    // Get data from remote, then cache the result and finally return the result
                    itemsPublisher = itemsPublisher
                        .flatMap { items in
                            // Once the result have been retrieved, save it to cache and return it
                            return self.saveItems(items).flatMap { _ in Just(items) }
                        }
                        .eraseToAnyPublisher()
                }

                return itemsPublisher
            }
            .eraseToAnyPublisher()
    }

    private func saveItems(_ items: [Item]) -> AnyPublisher<Void, Error> {
        return itemsDataSourceFactory.localItemsDataSource.saveItems(items)
    }
}
