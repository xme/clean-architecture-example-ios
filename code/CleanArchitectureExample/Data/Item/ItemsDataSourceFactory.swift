//
//  ItemsDataSourceFactory.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 7/6/22.
//

protocol ItemsDataSourceFactoryProtocol {

    var remoteItemsDataSource: RemoteItemsDataSourceProtocol { get }
    var localItemsDataSource: LocalItemsDataSourceProtocol { get }

    func locateDataSource(when validCache: Bool) -> ItemsDataSourceProtocol
}

struct ItemsDataSourceFactory: ItemsDataSourceFactoryProtocol {

    let remoteItemsDataSource: RemoteItemsDataSourceProtocol
    let localItemsDataSource: LocalItemsDataSourceProtocol

    init(remoteItemsDataSource: RemoteItemsDataSourceProtocol,
         localItemsDataSource: LocalItemsDataSourceProtocol) {
        self.remoteItemsDataSource = remoteItemsDataSource
        self.localItemsDataSource = localItemsDataSource
    }

    func locateDataSource(when validCache: Bool) -> ItemsDataSourceProtocol {
        if validCache {
            return localItemsDataSource
        } else {
            return remoteItemsDataSource
        }
    }
}
