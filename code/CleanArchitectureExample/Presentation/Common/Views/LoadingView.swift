//
//  LoadingView.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 8/6/22.
//

import UIKit

final class LoadingView: UIView {

    // MARK: - UI properties

    private lazy var visualEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .regular)
        let visualEffectView = UIVisualEffectView(effect: blurEffect)
        // Init for Autolayout
        visualEffectView.frame = .zero
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        return visualEffectView
    }()

    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
        // Init for Autolayout
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        return activityIndicatorView
    }()

    // MARK: - Initialization

    override init(frame: CGRect) { // For using custom view in code
        super.init(frame: frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) { // For using custom view in IB
        super.init(coder: aDecoder)
        setupView()
    }
}

private extension LoadingView {

    // MARK: - UI

    /// Configure the view
    func setupView() {
        addUIElements()
        addUIConstraints()
        setupSubviews()
    }

    /// Set UI elements
    func addUIElements() {
        addSubviews([
            visualEffectView,
            activityIndicatorView
        ])
    }

    /// Set UI constraints
    func addUIConstraints() {
        NSLayoutConstraint.activate([
            visualEffectView.leadingAnchor.constraint(equalTo: leadingAnchor),
            visualEffectView.topAnchor.constraint(equalTo: topAnchor),
            trailingAnchor.constraint(equalTo: visualEffectView.trailingAnchor),
            bottomAnchor.constraint(equalTo: visualEffectView.bottomAnchor),
            activityIndicatorView.centerXAnchor.constraint(equalTo: centerXAnchor),
            activityIndicatorView.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }

    /// Set up subviews
    func setupSubviews() {
        addGestureRecognizer(UITapGestureRecognizer()) // Block user interaction

        activityIndicatorView.startAnimating()
    }
}
