//
//  ItemsSceneBuilder.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 6/6/22.
//

struct ItemsSceneBuilder {

    static let shared = ItemsSceneBuilder()

    func build() -> ItemsViewController {
        let localItemsDataSource = LocalItemsDataSource()
        let remoteItemMapper = RemoteItemMapper()
        let remoteItemsDataSource = RemoteItemsDataSource(remoteItemMapper: remoteItemMapper)
        let itemsDataSourceFactory = ItemsDataSourceFactory(remoteItemsDataSource: remoteItemsDataSource, localItemsDataSource: localItemsDataSource)
        let itemsRepository = ItemsRepository(itemsDataSourceFactory: itemsDataSourceFactory)
        let getItemsUseCase = GetItemsUseCase(itemsRepository: itemsRepository)
        let viewModel = ItemsViewModel(getItemsUseCase: getItemsUseCase)
        return ItemsViewController(viewModel: viewModel)
    }
}
