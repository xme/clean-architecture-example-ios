//
//  ItemsViewController.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 3/6/22.
//

import Combine
import UIKit

final class ItemsViewController: UIViewController {

    // MARK: - Constants

    private enum Constants {
        static let estimatedHeight: CGFloat = 200.0
    }

    // MARK: - UI properties

    private lazy var tableView: UITableView = {
        let tableView = UITableView.initForAutolayout()
        return tableView
    }()

    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshDidPull(_:)), for: .valueChanged)
        return refreshControl
    }()

    private lazy var loadingView: LoadingView = {
        let loadingView = LoadingView.initForAutolayout()
        return loadingView
    }()

    // MARK: - Properties

    private var viewModel: ItemsViewModelProtocol
    private var cancellables: Set<AnyCancellable> = []
    private var items = [Item]()

    // MARK: - Initialization

    // For using the view controller in code
    init(viewModel: ItemsViewModelProtocol) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }

    // For using the view controller in a storyboard scene
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Set up views, taking into account that content will not be available until the view controller starts
        // listening to the view model.
        setupView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        clearsSelectedOnViewWillAppear()
        // Start listening to the view model in order to get data state and react accordingly
        bind(to: viewModel)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        // Stop listening to the view model, so no more data state changes are received by this view
        unbind()
        // Cancel all pending jobs in the view model, so that another view controller don't get results for operations
        // that it has not called.
        viewModel.clear()
    }
}

private extension ItemsViewController {

    // MARK: - UI

    /// Configure the view
    func setupView() {
        addUIElements()
        addUIConstraints()
        setupSubviews()
    }

    /// Set UI elements
    func addUIElements() {
        view.addSubviews([
            tableView,
            loadingView
        ])
    }

    /// Set UI constraints
    func addUIConstraints() {
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            view.trailingAnchor.constraint(equalTo: tableView.trailingAnchor),
            view.bottomAnchor.constraint(equalTo: tableView.bottomAnchor),
            loadingView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            loadingView.topAnchor.constraint(equalTo: view.topAnchor),
            view.trailingAnchor.constraint(equalTo: loadingView.trailingAnchor),
            view.bottomAnchor.constraint(equalTo: loadingView.bottomAnchor)
        ])
    }

    /// Set up subviews
    func setupSubviews() {
        view.backgroundColor = .systemBackground // Set background color for the whole screen

        navigationItem.title = "Fellowship of the Ring"

        tableView.register(ItemTableViewCell.self, forCellReuseIdentifier: ItemTableViewCell.reuseIdentifier)
        tableView.refreshControl = refreshControl
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Constants.estimatedHeight
        tableView.delegate = self
        tableView.dataSource = self

        loadingView.isHidden = true
    }

    // MARK: Helpers

    // Equivalent to property clearsSelectedOnViewWillAppear on a UITableViewController
    // See: https://stackoverflow.com/a/30688540/5189200
    func clearsSelectedOnViewWillAppear() {
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }

    @objc
    func refreshDidPull(_ sender: AnyObject) {
        // Stop all view model jobs, so that the refresh is performed with no pending results of other operations
        // (including a previous refresh).
        viewModel.clear()
        // Fetch items
        viewModel.getItems()
    }

    // MARK: - View model

    func bind(to viewModel: ItemsViewModelProtocol) {
        viewModel.itemsDriver
                 .sink { [weak self] in self?.handle(state: $0) }
                 .store(in: &cancellables)
    }

    func unbind() {
        cancellables.forEach { $0.cancel() }
        cancellables.removeAll()
    }
}

extension ItemsViewController: ItemsViewProtocol {
    func handle(state: ItemsState) {
        switch state {
        case .void:
            // View model has no data, so fetch wishlist
            viewModel.getItems()
        case .loading:
            loadingView.isHidden = false
        case .success(let items):
            self.items = items
            tableView.reloadData()
            loadingView.isHidden = true
            tableView.refreshControl?.endRefreshing()
        case .failure:
            loadingView.isHidden = true
            preconditionFailure("Unsupported state")
        }
    }
}

// MARK: - Table view data source

extension ItemsViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ItemTableViewCell.reuseIdentifier, for: indexPath) as! ItemTableViewCell
        let item = items[indexPath.row]
        cell.populate(with: item)
        return cell
    }
}

// MARK: - Table view delegate

extension ItemsViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let itemDetailViewController = ItemDetailSceneBuilder.shared.build(with: items[indexPath.row])
        navigationController?.pushViewController(itemDetailViewController, animated: true)
    }
}
