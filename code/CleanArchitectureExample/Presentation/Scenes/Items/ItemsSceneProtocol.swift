//
//  ItemsSceneProtocol.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 6/6/22.
//

import Combine

// MARK: - State

enum ItemsState {
    case void
    case loading
    case success(items: [Item])
    case failure(error: Error)
}

// MARK: - View

protocol ItemsViewProtocol {
    func handle(state: ItemsState)
}

extension ItemsViewProtocol {
    // All methods are optional, each view decides which states are observed depending on its needs
    func handle(state: ItemsState) {}
}

// MARK: - View model

protocol ItemsViewModelProtocol {
    var itemsDriver: Published<ItemsState>.Publisher { get }

    func getItems()
    func clear()
}

