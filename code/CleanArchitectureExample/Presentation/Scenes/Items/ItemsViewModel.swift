//
//  ItemsViewModel.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 6/6/22.
//

import Combine

final class ItemsViewModel: ObservableObject {

    @Published private var itemsState: ItemsState = .void
    private var cancellables: Set<AnyCancellable> = []
    private let getItemsUseCase: GetItemsUseCaseProtocol

    init(getItemsUseCase: GetItemsUseCaseProtocol) {
        self.getItemsUseCase = getItemsUseCase
    }

    private func clearAllJobs() {
        cancellables.forEach { $0.cancel() }
        cancellables.removeAll()
    }
}

extension ItemsViewModel: ItemsViewModelProtocol {

    var itemsDriver: Published<ItemsState>.Publisher { $itemsState }

    func getItems() {
        itemsState = .loading
        getItemsUseCase.execute(with: ())
            .sink(receiveCompletion: { [weak self] completion in
                switch completion {
                case .failure(let error):
                    self?.itemsState = .failure(error: error)
                case .finished:
                    break
                }
            }, receiveValue: { [weak self] items in
                self?.itemsState = .success(items: items)
            })
            .store(in: &cancellables)
    }

    func clear() {
        clearAllJobs()
    }
}
