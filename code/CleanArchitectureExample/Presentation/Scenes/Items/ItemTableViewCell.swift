//
//  ItemTableViewCell.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 3/6/22.
//

import UIKit

final class ItemTableViewCell: UITableViewCell {

    // MARK: - Constants

    private enum Constants {
        static let horizontalMargin: CGFloat = 24.0
        static let verticalMargin: CGFloat = 16.0
        static let horizontalSpacing: CGFloat = 16.0
        static let iconAspectRatio: CGFloat = 3.0 / 4.0
        static let iconHeight: CGFloat = 80.0
    }

    static let reuseIdentifier = "ItemTableViewCell"

    // MARK: - UI properties

    private lazy var contentStackView: UIStackView = {
        let view = UIStackView.initForAutolayout()
        view.axis = .horizontal
        view.spacing = Constants.horizontalSpacing
        return view
    }()

    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView.initForAutolayout()
        imageView.contentMode = .scaleAspectFit
        imageView.accessibilityIdentifier = AccessibilityIdentifiers.ItemTableViewCell.image
        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel.initForAutolayout()
        label.numberOfLines = 0
        label.accessibilityIdentifier = AccessibilityIdentifiers.ItemTableViewCell.title
        return label
    }()

    // MARK: - Initialization

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialConfig()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialConfig()
    }

    override func prepareForReuse() {
        iconImageView.image = nil
        titleLabel.text = nil
    }
}

private extension ItemTableViewCell {

    // MARK: - UI

    /// Configure the view
    func initialConfig() {
        addUIElements()
        addUIConstraints()
        setupSubviews()
    }

    /// Set UI elements
    func addUIElements() {
        contentStackView.addArrangedSubviews([
            iconImageView,
            titleLabel
        ])
        contentView.addSubviews([
            contentStackView
        ])
    }

    /// Set UI constraints
    func addUIConstraints() {
        NSLayoutConstraint.activate([
            // Content stack view
            contentStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.horizontalMargin),
            contentStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Constants.verticalMargin),
            contentView.trailingAnchor.constraint(equalTo: contentStackView.trailingAnchor, constant: Constants.horizontalMargin),
            contentView.bottomAnchor.constraint(equalTo: contentStackView.bottomAnchor, constant: Constants.verticalMargin),
            // Image view
            iconImageView.widthAnchor.constraint(equalTo: iconImageView.heightAnchor, multiplier: Constants.iconAspectRatio),
            iconImageView.heightAnchor.constraint(equalToConstant: Constants.iconHeight)
        ])
    }

    /// Set up subviews
    func setupSubviews() {
        contentView.backgroundColor = .clear
    }
}

extension ItemTableViewCell {

    // MARK: - Content

    func populate(with item: Item) {
        titleLabel.text = item.name
        iconImageView.load(url: item.url)
    }
}
