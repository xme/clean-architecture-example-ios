//
//  ItemDetailViewController.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 6/6/22.
//

import UIKit

final class ItemDetailViewController: UIViewController {

    // MARK: - Constants

    private enum Constants {
        static let horizontalMargin: CGFloat = 24.0
        static let verticalMargin: CGFloat = 16.0
        static let verticalSpacing: CGFloat = 20.0
        static let cardAspectRatio: CGFloat = 3.0 / 4.0
    }

    // MARK: - UI properties

    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView.initForAutolayout()
        return scrollView
    }()

    private lazy var contentView: UIView = {
        let view = UIView.initForAutolayout()
        return view
    }()

    private lazy var contentStackView: UIStackView = {
        let view = UIStackView.initForAutolayout()
        view.axis = .vertical
        view.spacing = Constants.verticalSpacing
        return view
    }()

    private lazy var cardImageView: UIImageView = {
        let imageView = UIImageView.initForAutolayout()
        imageView.contentMode = .scaleAspectFit
        imageView.accessibilityIdentifier = AccessibilityIdentifiers.ItemDetail.image
        return imageView
    }()

    // MARK: - Properties

    let item: Item

    // MARK: - Initialization

    init(item: Item) {
        self.item = item

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupContent()
    }
}

private extension ItemDetailViewController {

    // MARK: - UI

    /// Configure the view
    func setupView() {
        addUIElements()
        addUIConstraints()
        setupSubviews()
    }

    /// Set UI elements
    func addUIElements() {
        contentStackView.addArrangedSubview(cardImageView)
        contentView.addSubview(contentStackView)
        scrollView.addSubview(contentView)
        view.addSubview(scrollView)
    }

    /// Set UI constraints
    func addUIConstraints() {
        NSLayoutConstraint.activate([
            // Scroll view
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            view.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            view.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            // Content view
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            scrollView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            contentView.widthAnchor.constraint(equalTo: scrollView.frameLayoutGuide.widthAnchor),
            // Content stack view
            contentStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.horizontalMargin),
            contentStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Constants.verticalMargin),
            contentView.trailingAnchor.constraint(equalTo: contentStackView.trailingAnchor, constant: Constants.horizontalMargin),
            contentView.bottomAnchor.constraint(equalTo: contentStackView.bottomAnchor, constant: Constants.verticalMargin),
            // Image view
            cardImageView.widthAnchor.constraint(equalTo: cardImageView.heightAnchor, multiplier: Constants.cardAspectRatio)
        ])
    }

    /// Set up subviews
    func setupSubviews() {
        view.backgroundColor = .systemBackground // Set background color for the whole screen
    }

    // MARK: - Content

    func setupContent() {
        navigationItem.title = item.name
        cardImageView.load(url: item.url)
    }
}
