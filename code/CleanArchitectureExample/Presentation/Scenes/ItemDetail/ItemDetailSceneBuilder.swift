//
//  ItemDetailSceneBuilder.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 8/6/22.
//

struct ItemDetailSceneBuilder {

    static let shared = ItemDetailSceneBuilder()

    func build(with item: Item) -> ItemDetailViewController {
        let itemDetailViewController = ItemDetailViewController(item: item)
        return itemDetailViewController
    }
}
