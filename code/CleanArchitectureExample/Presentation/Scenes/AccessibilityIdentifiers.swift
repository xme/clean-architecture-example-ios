//
//  AccessibilityIdentifiers.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 9/6/22.
//

enum AccessibilityIdentifiers {

    enum ItemTableViewCell {
        static let image = "Item table view cell image"
        static let title = "Item table view cell title"
    }

    enum ItemDetail {
        static let image = "Item detail image"
    }
}

