//
//  UIImageView+Kingfisher.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 9/6/22.
//

import UIKit
import Kingfisher

extension UIImageView {
    // Do not use downsampling processor, because it needs to receive image bounds size with a valid value.
    func load(url: String) {
        let url = URL(string: url)
//        let processor = DownsamplingImageProcessor(size: self.bounds.size)
        self.kf.indicatorType = .activity
        self.kf.setImage(
            with: url,
//            placeholder: UIImage(named: "placeholderImage"),
            options: [
//                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage,
                .cacheSerializer(FormatIndicatedCacheSerializer.png)
            ],
            completionHandler: { result in
//                switch result {
//                case .success(let value):
//                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
//                case .failure(let error):
//                    print("Job failed: \(error.localizedDescription)")
//                }
            }
        )
    }
}
