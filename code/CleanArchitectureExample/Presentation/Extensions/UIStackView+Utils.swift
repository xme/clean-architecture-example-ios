//
//  UIStackView+Utils.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 3/6/22.
//

import UIKit

extension UIStackView {

    func addArrangedSubviews(_ views: [UIView]) {
        views.forEach { addArrangedSubview($0) }
    }
}
