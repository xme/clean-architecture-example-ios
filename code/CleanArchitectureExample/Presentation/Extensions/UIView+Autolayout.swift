//
//  UIView+Init.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 3/6/22.
//

import UIKit

extension UIView {

    static func initForAutolayout() -> Self {
        let view = Self.init(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    func addSubviews(_ views: [UIView]) {
        views.forEach { addSubview($0) }
    }
}
