//
//  RemoteItem.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 11/6/22.
//

// Representation of the remote item. Usually an item obtained from a remote API accessed using HTTPS.

struct ItemsResponse: Codable {
    let items: [RemoteItem]
}

struct RemoteItem: Codable {
    let name: String
    let url: String
}
