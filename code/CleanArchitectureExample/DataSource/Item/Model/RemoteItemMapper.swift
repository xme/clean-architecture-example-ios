//
//  RemoteItemMapper.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 11/6/22.
//

struct RemoteItemMapper: MapperProtocol {

    // swiftlint:disable type_name
    typealias I = RemoteItem
    typealias O = Item
    // swiftlint:enable type_name

    func map(_ inputType: I) -> O {
        return Item(name: inputType.name,
                    url: inputType.url)
    }
}
