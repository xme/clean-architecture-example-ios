//
//  ItemSource.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 3/6/22.
//

struct ItemSource {

    static let itemsResponse = """
        {
          "items": [
            {
              "name": "Gandalf",
              "url": "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/Revised-Core-Set/Gandalf.jpg"
            },
            {
              "name": "Frodo",
              "url": "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/Conflict-at-the-Carrock/Frodo-Baggins.jpg"
            },
            {
              "name": "Aragorn",
              "url": "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/Revised-Core-Set/Aragorn.jpg"
            },
            {
              "name": "Gimli",
              "url": "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/Revised-Core-Set/Gimli.jpg"
            },
            {
              "name": "Legolas",
              "url": "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/Revised-Core-Set/Legolas.jpg"
            },
            {
              "name": "Sam",
              "url": "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/The-Black-Riders/Sam-Gamgee.jpg"
            },
            {
              "name": "Merry",
              "url": "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/The-Black-Riders/Merry.jpg"
            },
            {
              "name": "Pippin",
              "url": "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/The-Black-Riders/Pippin.jpg"
            },
            {
              "name": "Boromir",
              "url": "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/Heirs-of-Numenor/Boromir.jpg"
            }
          ]
        }
    """
/*
    // https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/Revised-Core-Set/Gandalf.jpg
    // https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/Conflict-at-the-Carrock/Frodo-Baggins.jpg
    // https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/Revised-Core-Set/Aragorn.jpg
    // https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/Revised-Core-Set/Gimli.jpg
    // https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/Revised-Core-Set/Legolas.jpg
    // https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/The-Black-Riders/Sam-Gamgee.jpg
    // https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/The-Black-Riders/Merry.jpg
    // https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/The-Black-Riders/Pippin.jpg
    // https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/Heirs-of-Numenor/Boromir.jpg
    static func getItems() -> [Item] {
        var items = [Item]()
        items.append(Item(name: "Gandalf", url: "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/Revised-Core-Set/Gandalf.jpg" ))
        items.append(Item(name: "Frodo", url: "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/Conflict-at-the-Carrock/Frodo-Baggins.jpg" ))
        items.append(Item(name: "Aragorn", url: "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/Revised-Core-Set/Aragorn.jpg" ))
        items.append(Item(name: "Gimli", url: "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/Revised-Core-Set/Gimli.jpg" ))
        items.append(Item(name: "Legolas", url: "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/Revised-Core-Set/Legolas.jpg" ))
        items.append(Item(name: "Sam", url: "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/The-Black-Riders/Sam-Gamgee.jpg" ))
        items.append(Item(name: "Merry", url: "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/The-Black-Riders/Merry.jpg" ))
        items.append(Item(name: "Pippin", url: "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/The-Black-Riders/Pippin.jpg" ))
        items.append(Item(name: "Boromir", url: "https://s3.amazonaws.com/hallofbeorn-resources/Images/Cards/Heirs-of-Numenor/Boromir.jpg" ))
        return items
    }
*/
}
