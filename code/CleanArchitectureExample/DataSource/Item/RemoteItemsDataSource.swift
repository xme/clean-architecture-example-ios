//
//  RemoteItemsDataSource.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 7/6/22.
//

import Combine
import Dispatch
import Foundation

struct RemoteItemsDataSource: RemoteItemsDataSourceProtocol {

    enum RemoteError: Error, Equatable {
        case jsonDecodingError
    }

    let remoteItemMapper: RemoteItemMapper

    init(remoteItemMapper: RemoteItemMapper) {
        self.remoteItemMapper = remoteItemMapper
    }

    func getItems() -> AnyPublisher<[Item], Error> {
        return Deferred { () -> AnyPublisher<[Item], Error> in
            let json = ItemSource.itemsResponse
            let decoder = JSONDecoder()
            guard let data = json.data(using: .utf8),
                  let itemsResponse = try? decoder.decode(ItemsResponse.self, from: data) else {
                return Fail(error: RemoteError.jsonDecodingError).eraseToAnyPublisher()
            }
            let items = itemsResponse.items.map { remoteItemMapper.map($0) }
            return Just(items).setFailureType(to: Error.self).eraseToAnyPublisher()
        }
        .delay(for: .seconds(2), scheduler: DispatchQueue.global()) // For simulating network delay
        .eraseToAnyPublisher()
    }
}
