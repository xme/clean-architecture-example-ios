//
//  ItemsDataSourceProtocols.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 7/6/22.
//

import Combine

protocol ItemsDataSourceProtocol {

    func getItems() -> AnyPublisher<[Item], Error>
}

protocol RemoteItemsDataSourceProtocol: ItemsDataSourceProtocol {}

protocol LocalItemsDataSourceProtocol: ItemsDataSourceProtocol {

    func isValidCache() -> AnyPublisher<Bool, Error>
    // RX Completable has no direct equivalent in Combine, so AnyPublisher<Void, Never> and Combine flatMap operator as
    // RX andThen operator will be used.
    // See: https://medium.com/perry-street-software-engineering/rxjava-completable-equivalents-in-apples-combine-framework-3f2faff4bf9d
    func saveItems(_ items: [Item]) -> AnyPublisher<Void, Error>
    func clear() -> AnyPublisher<Void, Error>
}
