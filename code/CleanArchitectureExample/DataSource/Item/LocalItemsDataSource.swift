//
//  LocalItemsDataSource.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 7/6/22.
//

import Combine
import Foundation

final class LocalItemsDataSource: LocalItemsDataSourceProtocol {

    enum CacheError: Error, Equatable {
        case cacheNotAvailableError
    }

    private enum Constants {
        static let cacheDuration = 30.0
    }

    var lastSaveDate: Date?
    var items: [Item]?

    func isValidCache() -> AnyPublisher<Bool, Error> {
        let result = Deferred { [weak self] () -> Result<Bool, Error>.Publisher in
            let valid: Bool
            if let lastSaveDate = self?.lastSaveDate {
                valid = Date.now.timeIntervalSince(lastSaveDate) <= Constants.cacheDuration
            } else {
                valid = false
            }
            return Just(valid).setFailureType(to: Error.self)
        }.eraseToAnyPublisher()

        return result
    }

    func saveItems(_ items: [Item]) -> AnyPublisher<Void, Error> {
        return Deferred { [weak self] () -> Result<Void, Error>.Publisher in
            self?.lastSaveDate = Date.now
            self?.items = items
            return Just(()).setFailureType(to: Error.self)
        }.eraseToAnyPublisher()
    }

    func clear() -> AnyPublisher<Void, Error> {
        return Deferred { [weak self] () -> Result<Void, Error>.Publisher in
            self?.lastSaveDate = nil
            self?.items = nil
            return Just(()).setFailureType(to: Error.self)
        }.eraseToAnyPublisher()
    }

    func getItems() -> AnyPublisher<[Item], Error> {
        guard let items = items else {
            return Fail(error: CacheError.cacheNotAvailableError).eraseToAnyPublisher()
        }
        return DeferredItems(with: items)
    }
}
