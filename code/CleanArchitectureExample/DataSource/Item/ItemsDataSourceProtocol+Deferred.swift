//
//  ItemsDataSourceProtocol+Deferred.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 7/6/22.
//

import Combine

extension ItemsDataSourceProtocol {

    func DeferredItems(with items: [Item]) -> AnyPublisher<[Item], Error> {
        // Deferred awaits subscription before running the supplied closure to create a publisher for the new subscriber.
        return Deferred {
            // Just() do not have error type, so it has to be "added". See: https://stackoverflow.com/questions/60550577/combine-convert-just-to-anypublisher
            Just(items).setFailureType(to: Error.self)
        }
        .eraseToAnyPublisher()
    }
}
