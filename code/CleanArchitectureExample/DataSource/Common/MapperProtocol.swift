//
//  MapperProtocol.swift
//  CleanArchitectureExample
//
//  Created by Xavier Mellado Esteban on 11/6/22.
//

protocol MapperProtocol {

    // swiftlint:disable type_name
    associatedtype I
    associatedtype O
    // swiftlint:enable type_name

    func map(_ inputType: I) -> O
}
