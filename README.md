# Clean Architecture Example

App that shows the CLEAN Architecture for Zara.

## Description

This project applies the Clean Architecture aimed to be used in Zara App. 
For academic purpose.

## Getting Started

### Dependencies

* Xcode 13 & UP

### Installing

* Resolve Packages 


## Authors

Xavier Mellado (@xmellado)


## License

This project is licensed under the [NAME HERE] License - see the LICENSE.md file for details

## Acknowledgments

* [awesome-readme](docs/CLEANArchitecture.md)