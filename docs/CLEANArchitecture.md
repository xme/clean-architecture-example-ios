Version: 1.3
Date: 2022/06/20

> **IMPORTANT**: This is a work in progress task. This is a refactor and a prototype with a proposal for a new architecture, which intends to start a discussion to evolve the approach and achieve a consensus. The code can change at any time and this documentation may not be in sync with the code.

**Changelog**

Version 1.3
 - Domain data class Wishlist was introduced.
 - Use cases have a third generic parameter for the error.

Version 1.2
 - Dependency injection was introduced in use cases, IZClient is injected using ZaraResolver.
 - Use cases were refactored to extract common factors and avoid as much duplication as possible.

Version 1.1
 - Added a second kind of view model that is more generic.
 - Use cases were split into separate classes for implementing the new kind of view model.

Version 1.0
 - Initial version.
 
# Refactor of view controllers with multiple responsibilities

View controllers with multiple responsibilities that contain view logic and data holding, managing and retrieval are really difficult to update and maintain. In order to solve this situation, a new architecture is proposed in this document and prototyped in the wish list view controller.

## Objectives

The ultimate goal is to have view controllers with a **single** responsibility: implement the view logic. To reach the goal, it is proposed:

1) View controllers should be freed from dealing with data retrieval and they should not use IZClient (and even ZaraContext). This will simplify view controllers and make them more testable.

2) A class that manages data and its state for the view controller must be created, so that a view controller only has the view logic for displaying data and sending user actions.

3) View controllers should be freed from creating instances using static methods (such as instantiate, create, etc.). A dedicated class for building (wiring) the scene should be created.

4) Move IZClient and ZaraContext to a class in charge of doing data retrieval, so that the view controller and the class managing data (2) can be tested.

# Architecture draft

In order to achieve the objectives, an incomplete (see below) implementation of [Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) with a [Model - View - ViewModel](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel) (MVVM) in the presentation layer is proposed. Both architectural patterns are usually combined in mobile software development in the following way:

 1. Presentation layer, where the user interface interacts with entities. This layer includes user interface, view model, and most of your native platform dependent code.
 2. Domain layer, where entities and the interaction with them is represented, using domain models (data objects) and use cases (operations on data objects). In general, each use case will define a single operation on an entity, such as create, read, update or delete (CRUD). At the same time, this use case operation will use one or more operations on one or more repositories (but, most of the time a use case only uses one operation of one repository). Repositories contain all the operations available on an entity.
 3. Data layer, where repositories are implemented and usually remote (network) and local (cache) sources of data are managed.
 4. Data source layer, where the remote and local versions of a repository are implemented.

> Previous explanation is really simplified on purpose, because a comprehensive explanation on the subject is out of the scope of this document. You may find many holes that raise lots of questions, just ask!

In this proposal, there are a two points that **differ** with the usual approach in Clean Architecture:

 1. The Clean Architecture implementation is **incomplete**, because there are no repositories for the time being, but in the future they will be added. Currently, the view model will interact with IZClient and ZaraContext through use cases, removing these *nasty* classes from view controllers. Later, repositories can be created to replace that interaction in a transparent way for the view model, in particular, and the presentation layer, in general.
 2. Clean Architecture defines that: a) each layer must have its own representation of an entity (data object), and b) entity representations must be mapped when *traveling* through layers. In practice, this is really a burden. Up to 4 or 5 different objects may be used to represent a given entity, with their respective mappers (from data source to data, from data to domain, from domain to presentation). This approach is a source of frustration for the developer, which has to create very similar data objects and mappers, and a source of bugs, since the tedious task of mapping is prone to generate errors. In this proposal, **only the data source layer and the domain layer have data objects**, so an entity may have: a) *network* and *cache* objects in data sources layer, to deal with network calls and database or file persistence, and b) **a *domain* object shared across data, domain and presentation layers**. Additionally,  a model for a view can be created in the presentation layer, if needed (see section [*Model for the view*](#model-for-the-view)).

The proposal can be summarized in a diagram:

<a href="https://ibb.co/hdpwdPM"><img src="https://i.ibb.co/X5mM5h4/Clean-architecture-drawio.png" alt="Clean-architecture-drawio" border="0"></a>

## Why not VIPER (presenters)? 

View-Interactor-Presenter-Entity-Router uses a presenter for managing data. Presenters have a reference to the view, and they are usually **tightly coupled to its view**, because view logic is moved to the presenter, and despite using dependency inversion with protocols to communicate view and presenter.

The idea of moving all view logic to the presenter makes the presenter not reusable or shareable (up to some point), and it usually ends up creating long protocols and boilerplate code due to the back-and-forth calls between view and presenter for executing the view's logic.

## Structure of the proposal

There are two problems to tackle. First problem is how to split all the responsibilities the view controller currently has. Second problem is how to minimize code dependency on IZClient and ZaraContext. To this end, three new classes are proposed, and each one will deal with one major responsibility:

1) Some kind of factory or **builder** is needed to create and wire the different classes composing a scene. This class can be a singleton, and it is the perfect candidate to hold and inject the ZaraContext instead of the view controller. There should be **no** instantiation of classes composing the architecture outside this class.

> Note: currently, the injection of the IZClient can be done with the ZaraResolver. In the future, the ZaraContext or its sub-components may be included in the ZaraResolver, too. It's a work in progress, stay tuned...

2) A **view model** will be used to hold and manage data.

3) A **use case** will be used for data retrieval, as a sort of IZClient wrapper that only exposes the minimum set of IZClient methods (or properties) needed for modeling the entity managed. If something from ZaraContext is also needed, it should be hidden behind the use case, too.

## The builder

A builder singleton object can be used to hold the ZaraContext (more on this below) and create the view controller initialized with the view model, in a similar way as:

	struct WishlistBuilder {

	    static let shared = WishlistBuilder()
	    private static weak var context: ZaraContext?

	    func build(with context: ZaraContext) -> WishlistViewController {
	        WishlistBuilder.context = context

	        let viewController = instantiateViewController()
	        viewController.viewModel = instantiateViewModel()

	        return viewController
	    }

	    private func instantiateViewController() -> WishlistViewController {
	        let storyboard = UIStoryboard(name: "WishlistViewController",
	                                      bundle: Bundle(for: WishlistViewController.self))
	        return storyboard.instantiateInitialViewController() as! WishlistViewController
	    }

	    private func instantiateViewModel() -> WishlistViewModel {
	        return WishlistViewModel(getWishlistUseCase: GetWishlistUseCase(),
	                                 updateWishlistUseCase: UpdateWishlistUseCase(),
	                                 addWishlistItemsUseCase: AddWishlistItemsUseCase(),
	                                 deleteWishlistItemsUseCase: DeleteWishlistItemsUseCase())
	    }
	}

In the previous case, the view controller was created using code. However, if the view controller is not instantiated and it comes from `func prepare(for segue: UIStoryboardSegue, sender _: Any?)`, the builder can be used to configure the view controller:

    func configure(_ wishlistCollectionViewController: WishlistCollectionViewController,
                   withOnEditSelectionChanged onEditSelectionChanged: @escaping () -> Void,
                   onProductDetailDismissed: @escaping () -> Void,
                   viewingWishlistAsGuest: Bool,
                   itemsAsGuest: [WishlistItemWithDetail]?,
                   headerTitleAsGuest: String) {
        guard let context = WishlistBuilder.context else { return }
        wishlistCollectionViewController.context = context
        wishlistCollectionViewController.onEditSelectionChanged = onEditSelectionChanged
        wishlistCollectionViewController.onProductDetailDismissed = onProductDetailDismissed
        if viewingWishlistAsGuest {
            wishlistCollectionViewController.setSharedWishlistItems(itemsAsGuest, headerTitleAsGuest)
        }
    }

With this approach, the ZaraContext is not stored by the view controller, but the context can be still used to create other view controllers, either by instantiation or by configuration.

**IMPORTANT**: This approach will be revised thoroughly to take into account ZaraContext and IZClient life cycles, and to introduce dependency injection with the ZaraResolver.

## The view model

> A view model in MVVM and a model for the view are different things. See section [*Model for the view*](#model-for-the-view).

There are different interpretations of what a view model is. In this case, it is proposed a view model that adheres strictly to the definition of a view model by **knowing nothing from the view**, that is, **there is no reference to the view** in the view model, and there is **no** view logic of any kind (such as show or hide a loading view).

And then, it is possible to aim at two view model types:

1) In this proposal, the view model **manages only one kind of *entity* with all the available operations**, e.g.  wish list with the CRUD operations: read, update, add and remove elements.
As a consequence, it becomes a self-contained *component* that has everything needed to deal with the entity it manages, so it becomes **reusable** and **shareable**. The view model can be **reused by completely different view controllers**, since it doesn't know the view. Also, it can be **shared amongst view controllers**, because a view controller can fetch data with the view model, and then share this already initialized view model with another view controller, so that the second view controller does not trigger a second fetch (acting as a kind of cache).
This approach **minimizes the amount of code written and maximizes reusability**, but this shared view model can expose operations that are not needed in a particular view controller. For example, a screen that only reads data does not need the write operations available at the view model, so it does not comply, up to some point, with the idea of "easy to use correctly and hard to use incorrectly". Note that in this case, **a view controller can have more than one view model**.

2) The view model can be created **exclusively for a screen, exposing the minimum set of operations needed, and may manage more than one entity if needed**. That is, a screen may only need the read operations of two entities and the view model contains both entities with only get operations.
This approach **does not share state between screens** (which can cause unexpected and difficult to debug problems) and **maximizes correctness**, but the view model is neither reusable nor shareable. The same data may be managed at different view models, so the same code will be found in them, increasing code duplication. Note that in this case, **a view controller has only one view model**.

Both approaches are valid, one approach or the other will be better depending on the context, and it should be evaluated on a case-by-case basis (use your common sense and expertise). As a rule of thumb, it can be recommended that:

 - If there are only one or two places where a given pair of data and operation is needed, then both approaches can be used.
 - If there are three or more places where a given pair of data and operation is needed, then the first approach may pay off **to avoid duplication of code**.

### View model structure

The view model will hold and manage data for one or more entities (without any view logic). Therefore, there should be a way to obtain the data, apply operations to it, get the results of the operations and get the modified data. To this end, it is proposed to have:

 1. **Data models** representing the actual entity (e.g. user, order, product, etc.).
 
		struct Wishlist {
		    let name: String?
		    let items: [WishlistItemWithDetail]
		    var shareStatus: WishlistShareStatus
		    var publicShareToken: String?
		}
 2. **States** representing the data or operation status (e.g. void, loading, success and failure) , where the success and failure states will carry inside results.
 
		enum WishlistState {
		    case void
		    case loading
		    case success(wishlist: Wishlist)
		    case failure(error: Error)
		}
 3. **Communication channels** to publish the state of operations to the view.
 
 		protocol WishlistViewModelProtocol {
			var wishlistDriver: Published<WishlistState>.Publisher { get }			
			[...]
		}
 4. **Functions** to request (CRUD) operations by the view.
 
 		protocol WishlistViewModelProtocol {
	 		[...]			
			func getWishlist()
		}

> Communication channels are implemented with reactive programming using Combine. See next sections [*Why reactive programming?*](#why-reactive-programming) and [*Why Combine?*](#why-combine) for an explanation.

The reason for using states (2) (with data (1) inside) and not returning only data models in communication channels is **to represent domain data state**. It is not enough for the view controller to receive a data model, it needs to know when:

 1. There is no data (void), in order to ask for it.
 2. Data is being retrieved (loading).
 3. Whether data has been successfully retrieved and its value (success).
 4. Whether an error has been found (error).

The view controller must know these states to properly implement view logic.

The reason for decoupling result returning (3) from function calling (4) is **to generalize as much as possible the view model**, in order to be able to use it in as many scenarios as possible:

 1. If the view model is shared between view controllers, each view controller will be a listener of the communication channel, and only one view controller (the active one interacting with the view model) will trigger operations, but **all listeners will get the updated data**.
 2. If the view model is not shared, one view controller will be the only listener and triggering operations.

The decoupling makes easy to share the view model (if needed), because if the function itself returns results (for example with callbacks, or await/async), it means that every time a view controller wants to get data, it has to call the function, triggering again the operation, something that in many situations, is not ideal.

> In the wish list example, the view model targets reusability, for sharing it in the future amongst the three view controllers that deal with wish lists. However, for the time time being, only the main view controller has been updated.

Summing up everything, the complete protocol proposed for the wish list view model is:

	enum WishlistState {
	    case void
	    case loading
	    case success(wishlist: Wishlist)
	    case failure(error: Error)
	}

	enum UpdateWishlistEventState {
	    case loading
	    case success(shareStatus: WishlistShareStatus, publicShareToken: String?)
	    case failure(error: Error)
	}

	enum AddWishlistItemsEventState {
	    case loading
	    case success(addedItems: [WishlistItemWithDetail])
	    case failure(error: Error)
	}

	enum DeleteWishlistItemsEventState {
	    case loading
	    case success(deletedItems: [WishlistItemWithDetail])
	    case failure(error: Error)
	}

	protocol WishlistViewModelProtocol {
		var wishlistDriver: Published<WishlistState>.Publisher { get }
		var updateWishlistSignal: AnyPublisher<UpdateWishlistEventState, Never> { get }
		var addWishlistItemsSignal: AnyPublisher<AddWishlistItemsEventState, Never> { get }
		var deleteWishlistItemsSignal: AnyPublisher<DeleteWishlistItemsEventState, Never> { get }
		
		func getWishlist()
		func updateWishlist(name: String?, wishlistShareStatus: WishlistShareStatus)
		func add(items: [WishlistItemWithDetail])
		func delete(items: [WishlistItemWithDetail])
	}

where:

- Functions allow the controller to modify the wish list.
- The wish list *driver* ([a name borrowed from RxSwift](https://github.com/ReactiveX/RxSwift/blob/main/Documentation/Traits.md#driver)) is a Combine *publisher* that **never ends**, that **publishes in the main thread** (this is forced in use cases, see [*The use case*](#the-use-case) section), and that **replays the last value published** when an observer subscribes/sinks.
- The wish list [*signals*](https://github.com/ReactiveX/RxSwift/blob/main/Documentation/Traits.md#signal) are similar to drivers, but they **do not replay the last value published** when an observer subscribes/sinks. They are events, not data that is observed.

Thus, a *driver* is **data that is observed to drive your UI**, while signals are **events to know the state of operations requested on that data**.

> Drivers usually have a *.void* state for indicating that data was never requested. Signals do not have such a state, since they are events listened to after requesting an operation.

An example of the wish list flow could be:

- The view controller subscribes to all channels in the view model.
- The driver sends the void state, so the view controller calls `getWishlist()` to obtain data.
- The driver sends the loading state.
- The driver sends the success state with the wish list inside.
- After a while, the user requests to make the wish list public, so the view controller calls `updateWishlist(name: String?,  wishlistShareStatus: WishlistShareStatus)` in the view model.
- The update signal sends the loading state.
- The update signal sends the success state with the response data.
- The **driver** (not the update signal) sends the success state with the updated wish list inside.

> Note that the view controller must react to every state that it receives.

### Why reactive programming?

If there is no reference to the view in a view model, the view can easily invoke operations in the view model, but how can the state of operations and data be returned to the view? There are 5 possibilities:

 1. _Key-Value Observing or KVO_
 2. _Functional Reactive Programming or FRP_
 3. _Delegation_
 4. _Boxing_
 5. _async/await_
[Source](https://www.raywenderlich.com/6733535-ios-mvvm-tutorial-refactoring-from-mvc#toc-anchor-005)

A deep discussion about the alternatives is out of the scope of this document (and my knowledge). But, AFAIK, (1) can be too verbose and cumbersome, (3) can derive in what is called *callback hell*, when chaining calls is needed (I have been there, and I don't want to visit that place again), (4) its capabilities are quite limited, like only accepting one listener per channel, (5) like 3 and 4, it is equivalent to having only one listener, because each time that the result is needed, the function must be called. Thus, leaving us only with (2) reactive programming (Combine, RxSwift, etc.), the most widely used and modern approach for implementing view models.

### Why combine?

[Combine](https://developer.apple.com/documentation/combine) has been selected as the reactive programming library, since: a) it is developed by Apple, b) it is integrated with several Foundation types, and c) the available set of publishers and operators should be enough for most of our necessities. There are other frameworks more mature like [RxSwift](https://github.com/ReactiveX/RxSwift), but its learning curve is steeper and many of its advantages may not be currently useful for the project.

### Model for the view

A view model in MVVM should not be confused with a model for a view (also called view model). The former is similar to a view controller in MVC or a presenter in MVP, a class that abstracts interactions with the model, while the latter is a *data* class (a class whose main purpose is to hold data) created specifically for the view, where its properties have a direct match with what it is needed to display on the screen, so that the logic in the view to process data from the domain layer is reduced to zero.

If a model for the view is needed, it is proposed to create a formatter/mapper/transformer that can be **used in the view controller** when data is received before updating views. With this approach, the view model will continue being **agnostic** of any view logic or particular need, and the view controller will not do the transformation by itself (leaving the responsibility to the formatter).

For example, a formatter could be used in the `WishlistViewController` when it handles wish list data, like this:

    func handle(state: WishlistState) {
        switch state {
        case .void:
            // View model has no data, so fetch wishlist
            viewModel.getWishlist()
        case .loading:
            izShowProgressHUD()
        case .success(let wishlist):
            let formattedData = myFormatter.format(wishlist: wishlist)
            updateWishlist(with: formattedData)
            izRemoveProgressHUD()
        case .failure:
            izRemoveProgressHUD()
            // FIXME: Do something with the error
            break
        }
    }

## The use case

Use cases (the equivalent of interactors in VIPER) implement available operations on the entity. In our app, these operations usually will require a IZClient or ZaraContext, which are huge classes that cannot be mocked, thus preventing any class using them from being tested. For this reason, the use case will be in charge of the following responsibilities:

1) Do one request or chain a few requests needed to accomplish a given operation. An example of the latter case can be: fetch an entity using an id, and then use the entity information to fetch other things that are the final result of the operation.

2) Control in which thread operations are executed and results are returned. In general, requests are performed in a background thread, and results are returned in the main thread in order to avoid problems when updating the UI.

> Note: thread management can be implemented in many different ways, for this reason there should be a clear policy (or rules), and adhere to them for preventing really weird bugs. Besides, it is mandatory to make sure that drivers and signals publish results **always** in the main thread.

In addition, for Zara app:

3) The IZClient or ZaraContext must be wrapped, exposing the minimum set of operations needed to interact with the entity, so that the client and the context are isolated from the view controller and view model.

4) Currently, there are **no clear clean architecture layers**, so request responses may be needed to map to domain entities. If the mapping is not trivial, like extracting a property, it is probably a better idea to move the responsibility to a mapper.

### Use case structure 

The basic structure of a use case is one function to perform the action and one function for stopping it. This may suggest that use cases can be modeled with a protocol using generics and type erasure. However, this has been proved to be difficult and a rigid solution that, most probably, will not scale well in a huge app like Zara with many different contexts and needs. Therefore, a more flexible approach is introduced, also allowing a IZClient wrapping, which can be used in most use cases:

 1. A general protocol for all use case common properties or functions.
 
		protocol UseCaseProtocol {
			func cancel()
		}
 2. An abstract class for modeling the general structure of a use case.
 
		// P: parameters data object
		// R: result
		// E: error
		class UseCase<P, R, E>: UseCaseProtocol where E: Error {

		    private let executionScheduler: DispatchQueue
		    private let publicationScheduler: DispatchQueue

		    init(executionScheduler: DispatchQueue = Scheduler.backgroundScheduler,
		         publicationScheduler: DispatchQueue = Scheduler.mainScheduler) {
		        self.executionScheduler = executionScheduler
		        self.publicationScheduler = publicationScheduler
		    }

		    // MARK: Execution

		    // Execute function cannot be overridden, since it is the template method. Subclasses will define what will be
		    // published implementing buildUseCasePublisher.
		    // See: https://en.wikipedia.org/wiki/Template_method_pattern
		    final func execute(with params: P) -> AnyPublisher<R, E> {
		        return buildUseCasePublisher(with: params)
		            // Execute requests in background thread
		            .subscribe(on: executionScheduler)
		            // Publish results in main thread
		            .receive(on: publicationScheduler)
		            // Apply type erasure on the chain of operators returning a AnyPublisher
		            .eraseToAnyPublisher()
		    }

		    // Helper method to be overridden by subclasses
		    func buildUseCasePublisher(with params: P) -> AnyPublisher<R, E> {
		        fatalError("buildUseCasePublisher() must be overridden in child classes. This is an abstract method.")
		    }

		    // MARK: Cancellation

		    // Method to be overridden by subclasses
		    func cancel() {
		        fatalError("cancel() must be overridden in child classes. This is an abstract method.")
		    }
		}
 3. A partial implementation of the use case abstract class for wrapping the IZClient.
		
		class ZaraClientUseCase<P, R, E>: UseCase<P, R, E> where E: Error {

		    @Injected internal var client: IZClient
		    internal var request: IZClientRequest?

		    override func cancel() {
		        client.cancel(request)
		    }
		}
 4. A protocol for the particular use case to implement.

		protocol GetWishlistUseCaseProtocol: UseCaseProtocol {
		    func execute(with params: Void) -> AnyPublisher<Wishlist, Error>
		}
 5. And finally, the use case concrete implementation:

		final class GetWishlistUseCase: ZaraClientUseCase<Void, Wishlist, Error>,
		                                GetWishlistUseCaseProtocol {

		    private var mapper = WishlistDetailResponseMapper()

		    override func buildUseCasePublisher(with params: Void) -> AnyPublisher<Wishlist, Error> {
		        // Futures are executed as soon as they are created, with Deferred its execution is deferred until there is
		        // a subscription. See: https://stackoverflow.com/a/68962783/5189200
		        return Deferred {
		            Future { [weak self] promise in
		                guard let self = self else { return }
		                self.request = self.client.wishlist.wishlistDetail { response, _ in
		                    // Uses cases NEVER map, this is an exception due to the current architecture of the app.
		                    if let response = response {
		                        promise(Result.success(self.mapper.map(response)))
		                    } else {
		                        promise(Result.failure(WishlistError.noResponse))
		                    }
		                }
		            }
		        }
		        .eraseToAnyPublisher()
		    }
		}

Let's take a closer look to the structure:

 1. The general protocol has only the `cancel` function, since it is the only common factor to all use cases, because the `execution` function cannot have generic parameters. A protocol with generics "is not a type" from the point of view of Swift (even whether all parameters are made concrete with `typealias`), and type erasure cannot help here since not all use cases will be exactly the same.
 2. The abstract class uses the [template pattern](https://en.wikipedia.org/wiki/Template_method_pattern) for implementing thread control, leaving to concrete classes the implementation of `buildUseCasePublisher` and `cancel` methods.
 3. The partial implementation of the use case abstract class holds the IZClient object, and implements the `cancel` method (for a IZClient request).
 4. The particular get use case protocol may seem something disposable, since it only contains the `execution` function that will be defined nevertheless in the final concrete class (5), and it is not mandatory to compile and run, but it is **critical**, because: a) it defines the contract that will **enable testing** and dependency injection, the view model should never use a concrete use case such as `GetWishlistUseCase`, it must consume a protocol like `GetWishlistUseCaseProtocol` in order to inject a mock object when testing, and b) **trims everything exposed** by the concrete use case class (such as the IZClient or the `buildUseCasePublisher` function) leaving a clean interface from the view model point of view.
 5. The concrete class only needs to implement `buildUseCasePublisher` to build the Publisher object that will do the actual action. As a detail of implementation, the IZClient uses callbacks to deliver results, and use cases are implemented with Combine's *AnyPublisher*(s). As a consequence, it is needed to build a bridge between the callback and the publisher. One solution can be wrapping a callback with a *Future*. However, a *Future* is a promise, and it is executed as soon as it is created, so a *Deferred* could wrap the *Future* to delay its execution until there is a subscriber to the publisher.

In general, you should write only (4) and (5) to implement any use case.

# Responsibilities summary

| Class         | Responsibilities                                           |
|---------------|------------------------------------------------------------|
|ViewController | 1) View life cycle methods, 2) view logic            |
|Formatter* | Formatting domain model data exposed by the view model for displaying by the view controller           |
|ViewModel      | Domain data holding and management           |
|Use case       | 1) Data retrieval (using IZClient), 2) thread execution management, 3) ZaraContext/IZClient isolation |
|Builder        | 1) Scene building, 2) ZaraContext holding |

\* *Optional*

# References

**General**
https://swiftwithmajid.com/2020/02/05/building-viewmodels-with-combine-framework
https://medium.com/@mshcheglov/mvvm-design-pattern-with-combine-framework-on-ios-5ff911011b0b
https://www.raywenderlich.com/4161005-mvvm-with-combine-tutorial-for-ios
https://www.raywenderlich.com/6733535-ios-mvvm-tutorial-refactoring-from-mvc
https://developer.android.com/topic/libraries/architecture/viewmodel

**Issues**
https://swiftsenpai.com/swift/define-protocol-with-published-property-wrapper/
https://developer.apple.com/documentation/combine/using-combine-for-your-app-s-asynchronous-code
https://medium.com/perry-street-software-engineering/rxjava-completable-equivalents-in-apples-combine-framework-3f2faff4bf9d

**Testing**
https://www.swiftbysundell.com/articles/unit-testing-combine-based-swift-code/
https://www.swiftbysundell.com/articles/testing-error-code-paths-in-swift/
https://www.swiftbysundell.com/tips/gathering-test-coverage-in-xcode/

**Others**
https://quickbirdstudios.com/blog/rxswift-combine-transition-guide/